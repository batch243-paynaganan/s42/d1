const { rmSync } = require('fs');
const User=require('../models/User');
const bcrypt=require('bcrypt');
const auth=require('../auth');
const Courses = require('../models/Courses');

// *Check if email already exists
/* 
*steps:
*1.use mongoose 'find' method to find duplicate emails
*2.use .then method
*/
module.exports.checkEmailExists=(req,res,next)=>{
    return User.find({email:req.body.email})
    .then(result=>{
        // console.log(req.body.email)
        let message=``;
        if(result.length>0){
            message=`The ${req.body.email} is already taken, please use other email.`
            return res.send(message);
        }else{
            next();
        }
    })
}

module.exports.registerUser=(req,res)=>{
    // *creates variable "newUSer" and instantiates a new 'User' object using mongoose model
    // *uses the information from the req.body to provide the necessary info
    let newUser = new User({
        firstName:req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo
    })
    // *saves the created object to our database
    return newUser.save().then(user=>{
        console.log(user);
        res.send(`congrats sir/maam ${newUser.firstName}! You are now registered`)
    })
    .catch(err=>{
        console.log(err);
        res.send(`Sorry ${newUser.firstName}, there was an error during the registration. Please try again!`)
    })
}

module.exports.loginUser=(req,res)=>{
    // *the findOne method, returns the first record in the collection that matches the search criteria.
    return User.findOne({email: req.body.email})
    .then(result=>{
        if(result===null){
            res.send(`Your email: ${req.body.email} is not registered yet. Register first!`)
        // *the compareSync method is used to compare a non encrypted pw from the login from the encrypted passwrd retrieve. it will return true or false value depending on the result
        }else{
            const isPasswordCorrect=bcrypt.compareSync(req.body.password, result.password)
            if(isPasswordCorrect){
                return res.send({accessToken: auth.createToken(result)})
            }else{
                return res.send('password incorrect')
            }}
    })
}

module.exports.userDetails=(req,res)=>{
    const id=req.body.id;
    return User.findById(id)
    .then(result=>{
        result.password=""
        return res.send(result)
    })
    .catch(err=>{
        return res.send(err)
    })
}

module.exports.profileDetails=(req,res)=>{
    // *user will be object that contains the id and email of the user that currently logged in
    const userData=auth.decode(req.headers.authorization);

    return User.findById(userData.id)
    .then(result=>{
        result.password="******"
        return res.send(result)
    }).catch(err=>{
        return res.send(err)
    })
}

module.exports.updateRole=(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    let idUpdate=req.params.userId;
    if(userData.isAdmin){
        return User.findById(idUpdate)
        .then(result=>{
            let update={isAdmin: !result.isAdmin}
            return User.findByIdAndUpdate(idUpdate,update,{new:true})
            .then(document=>{
                document.password="Confidential";
                return res.send(document)})
            .catch(err=>res.send(err))
        })
        .catch(err=>res.send(err))
    }else{
        return res.send("You don't have access to this page")
    }
}

module.exports.enroll=async(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    if(!userData.isAdmin){
        let courseId=req.params.courseId;
        let data={
            courseId: courseId,
            userId: userData.id,
        }
        let isCourseUpdated = await Courses.findById(courseId)
        .then(result=>{
            result.enrollees.push({userId: data.userId})
            result.slots--;
            return result.save()
            .then(success=>true)
            .catch(err=>false)
        })
        .catch(err=>false)

        let isUserUpdated = await User.findById(data.userId)
        .then(result=>{
            result.enrollments.push({courseId:data.courseId})
            return result.save()
            .then(success=>true)
            .catch(err=>false)
        })
        .catch(err=>false)
        return (isUserUpdated && isCourseUpdated) ? res.send('You are now enrolled') : res.send('Please try again')
    }else{
        return res.send('Access denied. Not a student.')
    }
}