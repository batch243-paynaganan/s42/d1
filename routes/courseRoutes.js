const express=require('express')
const router=express.Router()
const courseControllers=require('../controllers/courseControllers')
const auth=require('../auth.js')

// no params
router.post('/',auth.verify, courseControllers.addCourse) //add course
router.get('/allActiveCourses', courseControllers.getAllActive) //all active courses
router.get('/allCourses', auth.verify, courseControllers.getAllCourses) //all courses
// with params
router.get('/:courseId',courseControllers.getCourse) //specific course
router.put('/update/:courseId',auth.verify, courseControllers.updateCourse) //update one course
router.patch('/archive/:courseId',auth.verify, courseControllers.archiveCourses) //archive course

module.exports=router;